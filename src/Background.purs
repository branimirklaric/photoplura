module Background where

import Chrome (CHROME)
import Chrome.Downloads (download)
import Chrome.Runtime (OnInstalledReason(Install), id, onInstalledAddListener, onMessageAddListener_, openOptionsPage')
import Chrome.Runtime.MessageSender (MessageSender, tab)
import Chrome.Storage (get, sync)
import Chrome.Tabs (active, create_, remove_, url)
import Chrome.Tabs (id) as Tabs
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Now (NOW, now)
import Data.Argonaut.Core (Json, toObject, toString)
import Data.DateTime (DateTime, diff)
import Data.DateTime.Instant (fromDateTime, instant, toDateTime, unInstant)
import Data.Maybe (Maybe(..))
import Data.Newtype (unwrap)
import Data.Number (fromString)
import Data.Options ((:=))
import Data.StrMap (lookup)
import Data.Time.Duration (Days, Milliseconds(..))
import Domy (DOM)
import Domy.Global (alert, open)
import Domy.Window (ALERT, WINDOW)
import Evercookie (EVERCOOKIE, getEvercookie, setEvercookie)
import Math (abs)
import Network.HTTP.Affjax (AJAX)
import Prelude (class Applicative, Unit, bind, discard, pure, show, unit, void, (#), ($), (*>), (/=), (<#>), (<=), (<>), (==), (>>=), (||))

nowUtcDateTime :: forall effects. Eff (now :: NOW | effects) DateTime
nowUtcDateTime = now <#> toDateTime

trialIsValid :: DateTime -> DateTime -> Boolean
trialIsValid evercookieDateTime currentDateTime =
    evercookieDateTime
    # (diff currentDateTime :: DateTime -> Days)
    # unwrap
    # abs
    # (_ <= 30.0)

evercookieKey :: String
evercookieKey = "photopluraEvercookie"

getPhotopluraEvercookie :: forall effects.
    (Maybe DateTime -> Eff effects Unit) -> Eff (evercookie :: EVERCOOKIE | effects) Unit
getPhotopluraEvercookie callback = getEvercookie evercookieKey \evercookie ->
    evercookie >>= fromString <#> Milliseconds >>= instant <#> toDateTime # callback

setPhotopluraEvercookie :: forall effects.
    DateTime -> Eff (now :: NOW, evercookie :: EVERCOOKIE | effects) Unit
setPhotopluraEvercookie cookie =
    cookie # fromDateTime # unInstant # unwrap # show # setEvercookie evercookieKey

isLocalId :: String -> Boolean
isLocalId extensionId = extensionId /= "okcaacdehlelgkedhdiflohkdmibgeji"

hasActiveLicense :: forall effects.
    (Boolean -> Eff (chrome :: CHROME, ajax :: AJAX | effects) Unit) ->
    Eff (chrome :: CHROME, ajax :: AJAX | effects) Unit
hasActiveLicense callback =
    sync >>= (get { licenseKey: "" } \{ licenseKey } -> callback $ licenseKey /= "")

tryReadString :: String -> Json -> Maybe String
tryReadString fieldKey message = message # toObject >>= lookup fieldKey >>= toString

doIfJust :: forall anything applicative. Applicative applicative =>
    Maybe anything -> (anything -> applicative Unit) -> applicative Unit
doIfJust maybe computation = case maybe of
    Nothing -> pure unit
    Just something -> computation something

downloadUrl :: forall effects. String -> Eff (chrome :: CHROME | effects) Unit
downloadUrl downloadUrl = download { url: downloadUrl }

openUrl :: forall effects. String -> Eff (chrome :: CHROME | effects) Unit
openUrl openUrl = create_ (url := openUrl <> active := false)

closeSendersTab :: forall effects. MessageSender -> Eff (chrome :: CHROME | effects) Unit
closeSendersTab sender = sender # tab # Tabs.id # remove_

processMessage :: forall effects. Json -> MessageSender -> Eff (chrome :: CHROME | effects) Unit
processMessage message sender = do
    doIfJust (tryReadString "downloadUrl" message) downloadUrl
    doIfJust (tryReadString "openUrl" message) openUrl
    doIfJust (tryReadString "downloadUrlAndCloseTab" message)
        (\url -> downloadUrl url *> closeSendersTab sender)
    doIfJust (tryReadString "openUrlAndCloseTab" message)
        (\url -> openUrl url *> closeSendersTab sender)

activationAlertText :: String
activationAlertText = "The trial period for Photoplura has run out."
    <> " Please subscribe to receive a license key to activate Photoplura."

setOnMessageListener :: forall effects.
    String ->
    DateTime ->
    Eff (dom :: DOM, chrome :: CHROME, chrome :: CHROME, now :: NOW, ajax :: AJAX, alert :: ALERT, window :: WINDOW | effects) Unit
setOnMessageListener extensionId evercookieDateTime = onMessageAddListener_ \message sender -> do
    currentDateTime <- nowUtcDateTime
    if isLocalId extensionId || trialIsValid evercookieDateTime currentDateTime
        then do
            processMessage message sender
        else hasActiveLicense case _ of
            true -> do
                processMessage message sender
            false -> do
                alert activationAlertText
                open "https://gumroad.com/l/photoplura" # void

setOnInstalledListener :: forall effects.
    String ->
    Eff (dom :: DOM, evercookie :: EVERCOOKIE, evercookie :: EVERCOOKIE, now :: NOW,
        chrome :: CHROME, chrome :: CHROME, chrome :: CHROME, ajax :: AJAX, alert :: ALERT, window :: WINDOW | effects) Unit
setOnInstalledListener extensionId = onInstalledAddListener \details -> do
    if details.reason == Install then openOptionsPage' else pure unit
    currentDateTime <- nowUtcDateTime
    getPhotopluraEvercookie case _ of
        Nothing -> do
            setPhotopluraEvercookie currentDateTime
            setOnMessageListener extensionId currentDateTime
        Just evercookieDateTime ->
            setOnMessageListener extensionId evercookieDateTime

main :: forall effects.
    Eff (dom :: DOM, evercookie :: EVERCOOKIE, evercookie :: EVERCOOKIE, now :: NOW,
        chrome :: CHROME, chrome :: CHROME, chrome :: CHROME, ajax :: AJAX, alert :: ALERT, window :: WINDOW | effects) Unit
main = id >>= setOnInstalledListener
