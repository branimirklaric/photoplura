module Domy where

import Control.Monad.Eff (kind Effect)

-- | DOM interaction effect type.
foreign import data DOM :: Effect
