module Utils.CssHelpers where

import Control.Bind ((>>=))
import Control.Monad.Eff (Eff)
import Data.Function ((#))
import Data.Unit (Unit)
import Domy (DOM)
import Domy.CssStyleDeclaration (setProperty)
import Domy.Global (unsafeGetElementById)
import Domy.HtmlElement (class IsHtmlElement, HtmlElement, style)

setElementStyleProperty :: forall element effects. IsHtmlElement element =>
    String -> String -> element -> Eff (dom :: DOM | effects) Unit
setElementStyleProperty property value element =
    element # style >>= setProperty property value

setVisibilityHidden :: forall element effects. IsHtmlElement element =>
    element -> Eff (dom :: DOM | effects) Unit
setVisibilityHidden element = setElementStyleProperty "visibility" "hidden" element

setVisiblityVisible :: forall element effects. IsHtmlElement element =>
    element -> Eff (dom :: DOM | effects) Unit
setVisiblityVisible element = setElementStyleProperty "visibility" "visible" element

setDisplayBlock :: forall element effects. IsHtmlElement element =>
    element -> Eff (dom :: DOM | effects) Unit
setDisplayBlock element = setElementStyleProperty "display" "block" element

setDisplayNone :: forall element effects. IsHtmlElement element =>
    element -> Eff (dom :: DOM | effects) Unit
setDisplayNone element = setElementStyleProperty "display" "none" element

setDisplayBlockToId :: forall effects. String -> Eff (dom :: DOM | effects) Unit
setDisplayBlockToId id = unsafeGetElementById id
    >>= \(element :: HtmlElement) -> setDisplayBlock element

setDisplayNoneToId :: forall effects. String -> Eff (dom :: DOM | effects) Unit
setDisplayNoneToId id = unsafeGetElementById id
    >>= \(element :: HtmlElement) -> setDisplayNone element
