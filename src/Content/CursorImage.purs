module Content.CursorImage where

import Chrome (CHROME)
import Chrome.Runtime (getUrl)
import Control.Applicative (pure)
import Control.Bind ((>>=))
import Control.Monad.Eff (Eff)
import Data.Function ((#))
import Data.Unit (Unit)
import Domy (DOM)
import Domy.Events.MouseEvent (MouseEvent, pageX, pageY)
import Domy.Global (body, createElement)
import Domy.HtmlElements.HtmlImageElement (HtmlImageElement, setSrc)
import Domy.Node (appendChild)
import Domy.Tag (imgTag)
import Prelude (bind, discard, show, (+), (-), (<>))
import Utils.CssHelpers (setElementStyleProperty)

-- Adds cursor img element to the document's body.
addCursorImage :: forall effects. Eff (chrome :: CHROME, dom :: DOM | effects) HtmlImageElement
addCursorImage = do
    body' <- body
    cursorImage <- imgTag # createElement >>= appendChild body'
    setElementStyleProperty "position" "absolute" cursorImage
    setElementStyleProperty "z-index" "9999" cursorImage
    setElementStyleProperty "visibility" "hidden" cursorImage
    iconUrl <- getUrl "Icons/photo-album-16.png"
    setSrc iconUrl cursorImage
    pure cursorImage

moveCursorImage :: forall effects.
    HtmlImageElement -> MouseEvent -> Eff (dom :: DOM | effects) Unit
moveCursorImage cursorImage mouseEvent = do
    setElementStyleProperty "left" (show (pageX mouseEvent + 10) <> "px") cursorImage
    setElementStyleProperty "top" (show (pageY mouseEvent - 10) <> "px") cursorImage
