module Content where

import Chrome (CHROME)
import Chrome.Runtime (sendMessage)
import Content.CursorImage (addCursorImage, moveCursorImage)
import Control.Bind (pure, (>>=))
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Util (foreach, ifDo, ifDoEff)
import Data.Eq ((==))
import Data.Function ((#))
import Data.Functor ((<#>))
import Data.Maybe (Maybe(..))
import Data.Options ((:=))
import Data.Unit (Unit)
import Domy (DOM)
import Domy.Element (hasAttribute, setAttribute)
import Domy.Event (preventDefault)
import Domy.EventTarget (addEventListener, addEventListener', removeEventListener, toEventListener)
import Domy.EventType (keydown, mouseleave, mousemove, mouseover)
import Domy.Events.KeyboardEvent (KeyboardEvent, code)
import Domy.Global (document, getElementsByTagName)
import Domy.HtmlCollection (HtmlCollection, toArray)
import Domy.HtmlDocument (activeElement)
import Domy.HtmlElement.HtmlTextAreaElement (isTextAreaElement)
import Domy.HtmlElements.HtmlImageElement (HtmlImageElement, src)
import Domy.HtmlElements.HtmlInputElement (isInputElement)
import Domy.MutationObserver (childList, mutationObserver, observe, subtree)
import Domy.Tag (imgTag)
import Options (Controls, getOptions)
import Prelude (bind, discard, not, (<>), (||))
import Utils.CssHelpers (setVisibilityHidden, setVisiblityVisible)

-- | Finds all images in the windows document.
allImages :: forall effects. Eff (dom :: DOM | effects) (HtmlCollection HtmlImageElement)
allImages = getElementsByTagName imgTag

shouldAttachListeners :: forall effects. Eff (dom :: DOM | effects) Boolean
shouldAttachListeners = document >>= activeElement >>= case _ of
    Nothing -> pure true
    Just htmlElement -> (isInputElement htmlElement
        || isTextAreaElement htmlElement) # not # pure

sendImageMessage :: forall message effects.
    HtmlImageElement ->
    String ->
    (String -> message) ->
    KeyboardEvent ->
    Eff (dom :: DOM, chrome :: CHROME | effects) Unit
sendImageMessage image keyCode messageConstructor keyboardEvent =
    ifDo (code keyboardEvent == keyCode) do
        preventDefault keyboardEvent
        image # src <#> messageConstructor >>= sendMessage

addMouseImageListeners :: forall otherOptions effects.
    Controls otherOptions ->
    HtmlImageElement ->
    HtmlImageElement ->
    Eff (dom :: DOM, dom :: DOM, dom :: DOM, chrome :: CHROME | effects) Unit
addMouseImageListeners controls cursorImage image  = do
    let moveCursorImage' = moveCursorImage cursorImage # toEventListener
    let saveImage' = sendImageMessage image controls.downloadCode { downloadUrl: _ } # toEventListener
    let openImage' = sendImageMessage image controls.openCode { openUrl: _ } # toEventListener
    let saveImageAndCloseTab' = sendImageMessage image controls.downloadAndCloseCode { downloadUrlAndCloseTab: _ } # toEventListener
    let openImageAndCloseTab' = sendImageMessage image controls.openAndCloseCode { openUrlAndCloseTab: _ } # toEventListener

    -- Add a mouseover listener that attaches a keydown listener.
    image # addEventListener' mouseover \_ -> ifDoEff shouldAttachListeners do
        document >>= addEventListener keydown saveImage'
        document >>= addEventListener keydown openImage'
        document >>= addEventListener keydown saveImageAndCloseTab'
        document >>= addEventListener keydown openImageAndCloseTab'
        ifDo controls.showCursorImage do
            image # addEventListener mousemove moveCursorImage'
            setVisiblityVisible cursorImage

    -- Add a mouseleave listener that removes the attached keydown listener.
    image # addEventListener' mouseleave \_ -> ifDoEff shouldAttachListeners do
        document >>= removeEventListener keydown saveImage'
        document >>= removeEventListener keydown openImage'
        document >>= removeEventListener keydown saveImageAndCloseTab'
        document >>= removeEventListener keydown openImageAndCloseTab'
        ifDo controls.showCursorImage do
            image # removeEventListener mousemove moveCursorImage'
            setVisibilityHidden cursorImage

addListenersToImages :: forall otherOptions effects.
    Controls otherOptions ->
    HtmlImageElement ->
    HtmlCollection HtmlImageElement ->
    Eff (dom :: DOM, dom :: DOM, dom :: DOM, chrome :: CHROME | effects) Unit
addListenersToImages controls cursorImage imageCollection =
    allImages >>= toArray >>= foreach \image -> do
        hasntPhotopluraAttribute <- image # hasAttribute "photoplura" <#> not
        ifDo hasntPhotopluraAttribute do
            image # setAttribute "photoplura" ""
            addMouseImageListeners controls cursorImage image

main :: forall effects.
    Eff (dom :: DOM, dom :: DOM, dom :: DOM, chrome :: CHROME | effects) Unit
main = do
    document' <- document
    imageCollection <- allImages
    cursorImage <- addCursorImage
    getOptions \options -> do
        mutationObserver (\_ _ -> addListenersToImages options cursorImage imageCollection)
            # observe document' (childList := true <> subtree := true)
        addListenersToImages options cursorImage imageCollection
