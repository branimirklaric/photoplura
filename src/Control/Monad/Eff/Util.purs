module Control.Monad.Eff.Util where

import Control.Monad.Eff (Eff, foreachE)
import Control.Monad.Eff.Class (class MonadEff)
import Data.Function (flip)
import Data.Unit (Unit, unit)
import Prelude (bind, pure)

-- | Executes a side effect for each element in the array.
-- | Flipped version of Control.Monad.Eff.foreachE.
foreach :: forall anything effects. (anything -> Eff effects Unit) -> Array anything -> Eff effects Unit
foreach = flip foreachE

ifDo :: forall monad effects. MonadEff effects monad => Boolean -> monad Unit -> monad Unit
ifDo shouldDo toDo = if shouldDo then toDo else pure unit

ifDoEff :: forall monad effects. MonadEff effects monad => monad Boolean -> monad Unit -> monad Unit
ifDoEff shouldDoM toDo = do
    shouldDo <- shouldDoM
    ifDo shouldDo toDo
