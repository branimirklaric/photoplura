"use strict";

exports.id = function (tab) {
    return tab.id
}

exports.createImpl = function (createProperties) {
    return function (callback) {
        return function () {
            chrome.tabs.create(createProperties, function (tab) {
                callback(tab)()
            })
        }
    }
}

exports.removeImpl = function (tabIdOrIds) {
    return function (callback) {
        return function () {
            chrome.tabs.remove(tabIdOrIds, callback)
        }
    }
}
