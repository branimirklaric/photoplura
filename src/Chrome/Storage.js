"use strict";

exports.sync = function () {
    return chrome.storage.sync
}

exports.get = function (defaultValues) {
    return function (callback) {
        return function (storageArea) {
            return function () {
                storageArea.get(defaultValues, function (values) {
                    callback(values)()
                })
            }
        }
    }
}

exports.set = function (values) {
    return function (storageArea) {
        return function () {
            storageArea.set(values)
        }
    }
}
