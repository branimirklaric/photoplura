"use strict";

exports.download = function (options) {
    return function () {
        chrome.downloads.download(options)
    }
}
