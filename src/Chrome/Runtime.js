"use strict";

exports.id = function () {
    return chrome.runtime.id
}

exports.sendMessage = function (message) {
    return function () {
        chrome.runtime.sendMessage(message)
    }
}

exports.onMessageAddListener = function (callback) {
    return function () {
        chrome.runtime.onMessage.addListener(function (message, messageSender, sendResponse) {
            return callback(message)(messageSender)(function (response) {
                sendResponse(response)()
            })()
        })
    }
}

exports.onInstalledAddListenerImpl = function (callback) {
    return function () {
        chrome.runtime.onInstalled.addListener(function (details) {
            callback(details)()
        })
    }
}

exports.openOptionsPage = function (callback) {
    return function () {
        chrome.runtime.openOptionsPage(function () {
            callback()
        })
    }
}

exports.getUrl = function (path) {
    return function () {
        return chrome.runtime.getURL(path)
    }
}
