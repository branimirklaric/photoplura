module Chrome.Downloads where

import Chrome (CHROME)
import Control.Monad.Eff (Eff)
import Data.Unit (Unit)

foreign import download :: forall effects.
    { url :: String } -> Eff (chrome :: CHROME | effects) Unit
