module Chrome.Storage where

import Chrome (CHROME)
import Control.Monad.Eff (Eff)
import Prelude (Unit)

foreign import data StorageArea :: Type

foreign import sync :: forall effects. Eff (chrome :: CHROME | effects) StorageArea

foreign import get :: forall defaultValues effects.
    Record defaultValues ->
    (Record defaultValues -> Eff (chrome :: CHROME | effects) Unit) ->
    StorageArea ->
    Eff (chrome :: CHROME | effects) Unit

foreign import set :: forall values effects.
    Record values -> StorageArea -> Eff (chrome :: CHROME | effects) Unit
