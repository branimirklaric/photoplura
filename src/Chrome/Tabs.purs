module Chrome.Tabs where

import Chrome (CHROME)
import Control.Monad.Eff (Eff)
import Data.Foreign (Foreign, toForeign)
import Data.Options (Option, Options, opt, options)
import Prelude (Unit, const, pure, unit, ($))

foreign import data Tab :: Type

foreign import id :: Tab -> Int

data CreateProperties

url :: Option CreateProperties String
url = opt "url"

active :: Option CreateProperties Boolean
active = opt "active"

foreign import createImpl :: forall effects.
    Foreign -> (Tab -> Eff effects Unit) -> Eff (chrome :: CHROME | effects) Unit

create :: forall effects.
    Options CreateProperties ->
    (Tab -> Eff effects Unit) ->
    Eff (chrome :: CHROME | effects) Unit
create createProperties callback = createImpl (options createProperties) callback

create_ :: forall effects. Options CreateProperties -> Eff (chrome :: CHROME | effects) Unit
create_ createProperties = create createProperties (const $ pure unit)

foreign import removeImpl :: forall effects.
    Foreign -> Eff effects Unit -> Eff (chrome :: CHROME | effects) Unit

remove :: forall effects. Int -> Eff effects Unit -> Eff (chrome :: CHROME | effects) Unit
remove tabId callback = removeImpl (toForeign tabId) callback

remove_ :: forall effects. Int -> Eff (chrome :: CHROME | effects) Unit
remove_ tabId = remove tabId (pure unit)

removeMany :: forall effects. Array Int -> Eff effects Unit -> Eff (chrome :: CHROME | effects) Unit
removeMany tabIds callback = removeImpl (toForeign tabIds) callback

removeMany_ :: forall effects. Array Int -> Eff (chrome :: CHROME | effects) Unit
removeMany_ tabIds = removeMany tabIds (pure unit)
