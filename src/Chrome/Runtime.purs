module Chrome.Runtime(
    id,
    sendMessage,
    onMessageAddListener,
    onMessageAddListener_,
    onInstalledAddListener,
    OnInstalledReason(..),
    OnInstalledDetails,
    openOptionsPage,
    openOptionsPage',
    getUrl)
where

import Chrome (CHROME)
import Chrome.Runtime.MessageSender (MessageSender)
import Control.Applicative (pure, (*>))
import Control.Bind (bind, (>>=))
import Control.Monad.Eff (Eff)
import Control.Monad.Except (runExcept)
import Control.Semigroupoid ((>>>))
import Data.Argonaut.Core (Json)
import Data.Either (fromRight)
import Data.Foreign (Foreign, readString, readUndefined)
import Data.Foreign.Index ((!))
import Data.Function ((#), ($))
import Data.Maybe (Maybe(..), fromJust)
import Data.Traversable (traverse)
import Data.Unit (Unit, unit)
import Partial.Unsafe (unsafePartial)
import Prelude (class Eq)

foreign import id :: forall effects. Eff (chrome :: CHROME | effects) String

foreign import sendMessage :: forall message effects.
    message -> Eff (chrome :: CHROME | effects) Unit

foreign import onMessageAddListener :: forall response effects.
    (Json -> MessageSender -> (response -> Eff effects Unit) -> Eff effects Boolean) ->
    Eff (chrome :: CHROME | effects) Unit

onMessageAddListener_ :: forall effects.
    (Json -> MessageSender -> Eff effects Unit) ->
    Eff (chrome :: CHROME | effects) Unit
onMessageAddListener_ callback =
    onMessageAddListener \message sender _ -> callback message sender *> pure false

data OnInstalledReason = Install | Update | ChromeUpdate | SharedModuleUpdate

derive instance eqOnInstalledReason :: Eq OnInstalledReason

type OnInstalledDetails = {
    reason :: OnInstalledReason,
    previousVersion :: Maybe String,
    id :: Maybe String
}

reasonFromString :: String -> Maybe OnInstalledReason
reasonFromString reason = case reason of
    "install" -> Just Install
    "update" -> Just Update
    "chrome_update" -> Just ChromeUpdate
    "shared_module_update" -> Just SharedModuleUpdate
    _ -> Nothing -- Should never happen

readDetails :: Foreign -> OnInstalledDetails
readDetails details = unsafePartial $ fromRight $ runExcept do
    reason <- details ! "reason" >>= readString
    prevVersion <- details ! "previousVersion" >>= readUndefined >>= traverse readString
    id' <- details ! "id" >>= readUndefined >>= traverse readString
    pure $ {
        reason: reason # reasonFromString # fromJust,
        previousVersion: prevVersion,
        id: id' }

foreign import onInstalledAddListenerImpl :: forall effects.
    (Foreign -> Eff (effects) Unit) -> Eff (chrome :: CHROME | effects) Unit

onInstalledAddListener :: forall effects.
    (OnInstalledDetails -> Eff effects Unit) -> Eff (chrome :: CHROME | effects) Unit
onInstalledAddListener listener = onInstalledAddListenerImpl (readDetails >>> listener)

foreign import openOptionsPage :: forall effects.
    Eff effects Unit -> Eff (chrome :: CHROME | effects) Unit

openOptionsPage' ::  forall effects. Eff (chrome :: CHROME | effects) Unit
openOptionsPage' = openOptionsPage $ pure unit

foreign import getUrl :: forall effects. String -> Eff (chrome :: CHROME | effects) String
