module Chrome.Runtime.MessageSender where

import Chrome.Tabs (Tab)

foreign import data MessageSender :: Type

foreign import id :: MessageSender -> String

foreign import url :: MessageSender -> String

foreign import tlsChannelId :: MessageSender -> String

foreign import tab :: MessageSender -> Tab
