"use strict";

exports.id = function (messageSender) {
    return messageSender.id
}

exports.url = function (messageSender) {
    return messageSender.url
}

exports.tlsChannelId = function (messageSender) {
    return messageSender.tlsChannelId
}

exports.tab = function (messageSender) {
    return messageSender.tab
}
