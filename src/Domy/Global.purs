module Domy.Global where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe, fromJust)
import Domy (DOM)
import Domy.Document (body, createElement, getElementById, getElementsByTagName) as Document
import Domy.Element (class IsElement)
import Domy.HtmlCollection (HtmlCollection)
import Domy.HtmlDocument (HtmlDocument)
import Domy.HtmlDocument (location) as HtmlDocument
import Domy.HtmlElements.HtmlBodyElement (HtmlBodyElement)
import Domy.Location (Location)
import Domy.Location (href) as Location
import Domy.Tag (Tag)
import Domy.Window (ALERT, WINDOW, Window)
import Domy.Window (alert, document, open, window) as Window
import Partial.Unsafe (unsafePartial)
import Prelude (Unit, (<#>), (>>=))

document :: forall effects. Eff (dom :: DOM | effects) HtmlDocument
document = Window.window >>= Window.document

body :: forall effects. Eff (dom :: DOM | effects) HtmlBodyElement
body = document >>= Document.body

alert :: forall effects. String -> Eff (dom :: DOM, alert :: ALERT | effects) Unit
alert message = Window.window >>= Window.alert message

open :: forall effects.
    String -> Eff (dom :: DOM, window :: WINDOW | effects) (Maybe Window)
open url = Window.window >>= Window.open url

location :: forall effects. Eff (dom :: DOM | effects) Location
location = document >>= HtmlDocument.location

href :: forall effects. Eff (dom :: DOM | effects) String
href = location >>= Location.href

getElementById :: forall element effects. IsElement element =>
    String -> Eff (dom :: DOM | effects) (Maybe element)
getElementById id = document >>= Document.getElementById id

unsafeGetElementById :: forall effects element. IsElement element =>
    String -> Eff (dom :: DOM | effects) element
unsafeGetElementById id = getElementById id <#> unsafePartial fromJust

getElementsByTagName :: forall element effects. IsElement element =>
    Tag element -> Eff (dom :: DOM | effects) (HtmlCollection element)
getElementsByTagName tag = document >>= Document.getElementsByTagName tag

createElement :: forall element effects. IsElement element =>
    Tag element -> Eff (dom :: DOM | effects) element
createElement element = document >>= Document.createElement element
