module Domy.HtmlDocument where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Domy (DOM)
import Domy.EventTarget (class IsEventTarget)
import Domy.HtmlElement (HtmlElement)
import Domy.Location (Location)
import Domy.Node (class IsNode)
import Domy.Utils (unsafeReadProtoTagged)
import Prelude ((<#>))
import Unsafe.Coerce (unsafeCoerce)

foreign import data HtmlDocument :: Type

readHtmlDocument :: forall anything. anything -> Maybe HtmlDocument
readHtmlDocument = unsafeReadProtoTagged "HTMLDocument"

instance isEventTargetHtmlDocument :: IsEventTarget HtmlDocument where
    toEventTarget = unsafeCoerce
    fromEventTarget = readHtmlDocument

instance isNodeHtmlDocument :: IsNode HtmlDocument where
    toNode = unsafeCoerce
    fromNode = readHtmlDocument

foreign import location :: forall effects.
    HtmlDocument -> Eff (dom :: DOM | effects) Location

foreign import activeElementImpl :: forall effects.
    HtmlDocument -> Eff (dom :: DOM | effects) (Nullable HtmlElement)

activeElement :: forall effects.
    HtmlDocument -> Eff (dom :: DOM | effects) (Maybe HtmlElement)
activeElement htmlDocument = activeElementImpl htmlDocument <#> toMaybe
