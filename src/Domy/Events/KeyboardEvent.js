"use strict";

exports.key = function (keyboardEvent) {
    return keyboardEvent.key
}

exports.code = function (keyboardEvent) {
    return keyboardEvent.code
}

exports.which = function (keyboardEvent) {
    return keyboardEvent.which
}
