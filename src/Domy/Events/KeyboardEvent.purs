module Domy.Events.KeyboardEvent where

foreign import data KeyboardEvent :: Type

-- | A non-empty Unicode character string containing the printable representation
-- | of the key, if available.
foreign import key :: KeyboardEvent -> String

-- | Returns a string representing a physical key on the keyboard. Not
-- | affected by keyboard layout or state of the modifier keys.
foreign import code :: KeyboardEvent -> String

foreign import which :: KeyboardEvent -> String
