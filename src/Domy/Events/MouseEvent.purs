module Domy.Events.MouseEvent where

foreign import data MouseEvent :: Type

foreign import pageX :: MouseEvent -> Int

foreign import pageY :: MouseEvent -> Int
