module Domy.Location where

import Control.Monad.Eff (Eff)
import Domy (DOM)

foreign import data Location :: Type

foreign import href :: forall effects. Location -> Eff (dom :: DOM | effects) String
