module Domy.EventTarget where

import Control.Monad.Eff (Eff, kind Effect)
import Data.Maybe (Maybe)
import Domy (DOM)
import Domy.Event (class IsEvent)
import Domy.EventType (EventType)
import Domy.HtmlElements.HtmlImageElement (HtmlImageElement, readHtmlImageElement)
import Prelude (Unit)
import Unsafe.Coerce (unsafeCoerce)

-- | EventTarget is an interface implemented by objects that can receive events
-- | and may have listeners for them.
foreign import data EventTarget :: Type

class IsEventTarget eventTarget where
    toEventTarget :: eventTarget -> EventTarget
    fromEventTarget :: EventTarget -> Maybe eventTarget

instance isEventTargetHtmlImageElement :: IsEventTarget HtmlImageElement where
    toEventTarget = unsafeCoerce
    fromEventTarget = readHtmlImageElement

foreign import data EventListener :: Type -> # Effect -> Type

foreign import toEventListener :: forall event effects return. IsEvent event =>
    (event -> Eff effects return) -> EventListener event effects

-- | Adds a listener to an event target.
foreign import addEventListener :: forall event eventTarget effects.
    IsEvent event => IsEventTarget eventTarget =>
    EventType event ->
    EventListener event effects ->
    eventTarget ->
    Eff (dom :: DOM | effects) Unit

addEventListener' :: forall event eventTarget return effects.
    IsEvent event => IsEventTarget eventTarget =>
    EventType event ->
    (event -> Eff effects return) ->
    eventTarget ->
    Eff (dom :: DOM | effects) Unit
addEventListener' eventType listener target =
    addEventListener eventType (toEventListener listener) target

-- | Removes a listener from an event target.
foreign import removeEventListener :: forall event eventTarget effects.
    IsEvent event => IsEventTarget eventTarget =>
    EventType event ->
    EventListener event effects ->
    eventTarget ->
    Eff (dom :: DOM | effects) Unit

-- | Dispatches an event from an event target.
foreign import dispatchEvent :: forall event eventTarget effects.
    IsEvent event => IsEventTarget eventTarget =>
    event -> eventTarget -> Eff (dom :: DOM | effects) Boolean
