module Domy.CssStyleDeclaration where

import Control.Monad.Eff (Eff)
import Domy (DOM)
import Prelude (Unit)

foreign import data CssStyleDeclaration :: Type

foreign import setProperty :: forall effects.
    String -> String -> CssStyleDeclaration -> Eff (dom :: DOM | effects) Unit
