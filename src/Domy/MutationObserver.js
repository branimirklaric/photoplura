"use strict";

exports.mutationObserver = function (callback) {
    return new MutationObserver(function (records, observer) {
        callback(records)(observer)()
    })
}

exports.observeImpl = function (isNode) {
    return function (node) {
        return function (init) {
            return function (observer) {
                return function () {
                    observer.observe(node, init)
                }
            }
        }
    }
}
