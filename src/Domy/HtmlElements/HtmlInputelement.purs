module Domy.HtmlElements.HtmlInputElement where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(..))
import Domy (DOM)
import Domy.Element (class IsElement)
import Domy.EventTarget (class IsEventTarget)
import Domy.HtmlElement (class IsHtmlElement, HtmlElement, fromHtmlElement)
import Domy.Node (class IsNode)
import Domy.Utils (unsafeReadTagged')
import Prelude (Unit)
import Unsafe.Coerce (unsafeCoerce)

foreign import data HtmlInputElement :: Type

readHtmlInputElement :: forall anything. anything -> Maybe HtmlInputElement
readHtmlInputElement = unsafeReadTagged' "HTMLInputElement"

instance isNodeHtmlInputElement :: IsNode HtmlInputElement where
    toNode = unsafeCoerce
    fromNode = readHtmlInputElement

instance isEventTargetHtmlInputElement :: IsEventTarget HtmlInputElement where
    toEventTarget = unsafeCoerce
    fromEventTarget = readHtmlInputElement

instance isElementHtmlInputElement :: IsElement HtmlInputElement where
    toElement = unsafeCoerce
    fromElement = readHtmlInputElement

instance isHtmlElementHtmlInputElement :: IsHtmlElement HtmlInputElement where
    toHtmlElement = unsafeCoerce
    fromHtmlElement = readHtmlInputElement

isInputElement :: HtmlElement -> Boolean
isInputElement element = case (fromHtmlElement element :: Maybe HtmlInputElement) of
    Nothing -> false
    Just _ -> true

foreign import value :: forall effects.
    HtmlInputElement -> Eff (dom :: DOM | effects) String

foreign import setValue :: forall effects.
    String -> HtmlInputElement -> Eff (dom :: DOM | effects) Unit

foreign import checked :: forall effects.
    HtmlInputElement -> Eff (dom :: DOM | effects) Boolean

foreign import setChecked :: forall effects.
    Boolean -> HtmlInputElement -> Eff (dom :: DOM | effects) Unit
