module Domy.HtmlElement.HtmlTextAreaElement where

import Data.Maybe (Maybe(..))
import Domy.Element (class IsElement)
import Domy.EventTarget (class IsEventTarget)
import Domy.HtmlElement (class IsHtmlElement, HtmlElement, fromHtmlElement)
import Domy.Node (class IsNode)
import Domy.Utils (unsafeReadTagged')
import Unsafe.Coerce (unsafeCoerce)

foreign import data HtmlTextAreaElement :: Type

readHtmlTextAreaElement :: forall anything. anything -> Maybe HtmlTextAreaElement
readHtmlTextAreaElement = unsafeReadTagged' "HTMLTextAreaElement"

instance isNodeHtmlTextAreaElement :: IsNode HtmlTextAreaElement where
    toNode = unsafeCoerce
    fromNode = readHtmlTextAreaElement

instance isEventTargetHtmlTextAreaElement :: IsEventTarget HtmlTextAreaElement where
    toEventTarget = unsafeCoerce
    fromEventTarget = readHtmlTextAreaElement

instance isElementHtmlTextAreaElement :: IsElement HtmlTextAreaElement where
    toElement = unsafeCoerce
    fromElement = readHtmlTextAreaElement

instance isHtmlElementHtmlTextAreaElement :: IsHtmlElement HtmlTextAreaElement where
    toHtmlElement = unsafeCoerce
    fromHtmlElement = readHtmlTextAreaElement

isTextAreaElement :: HtmlElement -> Boolean
isTextAreaElement element = case (fromHtmlElement element :: Maybe HtmlTextAreaElement) of
    Nothing -> false
    Just _ -> true
