"use strict";

exports.value = function (inputElement) {
    return function () {
        return inputElement.value
    }
}

exports.setValue = function (value) {
    return function (inputElement) {
        return function () {
            inputElement.value = value
        }
    }
}

exports.checked = function (inputElement) {
    return function () {
        return inputElement.checked
    }
}

exports.setChecked = function (checked) {
    return function (inputElement) {
        return function () {
            inputElement.checked = checked
        }
    }
}
