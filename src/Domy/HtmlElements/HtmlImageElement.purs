module Domy.HtmlElements.HtmlImageElement where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe)
import Domy (DOM)
import Domy.Utils (unsafeReadTagged')
import Prelude (Unit)

foreign import data HtmlImageElement :: Type

readHtmlImageElement :: forall anything. anything -> Maybe HtmlImageElement
readHtmlImageElement = unsafeReadTagged' "HTMLImageElement"

foreign import src :: forall effects.
    HtmlImageElement -> Eff (dom :: DOM | effects) String

foreign import setSrc :: forall effects.
    String -> HtmlImageElement -> Eff (dom :: DOM | effects) Unit
