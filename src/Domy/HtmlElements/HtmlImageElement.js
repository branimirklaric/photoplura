"use strict";

exports.src = function (imageElement) {
    return function () {
        return imageElement.src
    }
}

exports.setSrc = function (src) {
    return function (img) {
        return function () {
            img.src = src
        }
    }
}
