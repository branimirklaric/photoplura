module Domy.HtmlElements.HtmlBodyElement where 

import Data.Maybe (Maybe)
import Domy.Element (class IsElement)
import Domy.EventTarget (class IsEventTarget)
import Domy.HtmlElement (class IsHtmlElement)
import Domy.Node (class IsNode)
import Domy.Utils (unsafeReadTagged')
import Unsafe.Coerce (unsafeCoerce)

foreign import data HtmlBodyElement :: Type

readHtmlBodyElement :: forall anything. anything -> Maybe HtmlBodyElement
readHtmlBodyElement = unsafeReadTagged' "HTMLBodyElement"

instance isEventTargetImageElement :: IsEventTarget HtmlBodyElement where
    toEventTarget = unsafeCoerce
    fromEventTarget = readHtmlBodyElement

instance isNodeImageElement :: IsNode HtmlBodyElement where
    toNode = unsafeCoerce
    fromNode = readHtmlBodyElement

instance isElementHtmlImageElement :: IsElement HtmlBodyElement where
    toElement = unsafeCoerce
    fromElement = readHtmlBodyElement
    
instance isHtmlElementHtmlImageElement :: IsHtmlElement HtmlBodyElement where
    toHtmlElement = unsafeCoerce
    fromHtmlElement = readHtmlBodyElement
