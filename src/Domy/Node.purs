module Domy.Node where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(..))
import Domy (DOM)
import Domy.EventTarget (class IsEventTarget)
import Domy.HtmlElements.HtmlImageElement (HtmlImageElement, readHtmlImageElement)
import Domy.Utils (unsafeReadProtoTagged)
import Prelude (Unit)
import Unsafe.Coerce (unsafeCoerce)

foreign import data Node :: Type

readNode :: forall anything. anything -> Maybe Node
readNode = unsafeReadProtoTagged "Node"

instance isEventTargetNode :: IsEventTarget Node where
    toEventTarget = unsafeCoerce
    fromEventTarget = readNode

class IsEventTarget node <= IsNode node where
    toNode :: node -> Node
    fromNode :: Node -> Maybe node

instance isNodeNode :: IsNode Node where
    toNode = unsafeCoerce
    fromNode = Just

instance isNodeHtmlImageElement :: IsNode HtmlImageElement where
    toNode = unsafeCoerce
    fromNode = readHtmlImageElement

foreign import textContent :: forall node effects. IsNode node =>
    node -> Eff (dom :: DOM | effects) String

foreign import setTextContent :: forall node effects. IsNode node =>
    String -> node -> Eff (dom :: DOM | effects) Unit

foreign import appendChild :: forall parentNode childNode effects.
    IsNode parentNode => IsNode childNode =>
    parentNode -> childNode -> Eff (dom :: DOM | effects) childNode

foreign import parentNode :: forall parentNode childNode effects.
    IsNode parentNode => IsNode childNode =>
    childNode -> Eff (dom :: DOM | effects) parentNode
