module Domy.Element where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(..))
import Domy (DOM)
import Domy.EventTarget (class IsEventTarget)
import Domy.HtmlElements.HtmlImageElement (HtmlImageElement, readHtmlImageElement)
import Domy.Node (class IsNode)
import Domy.Utils (unsafeReadProtoTagged)
import Prelude (Unit, id)
import Unsafe.Coerce (unsafeCoerce)

foreign import data Element :: Type

readElement :: forall anything. anything -> Maybe Element
readElement = unsafeReadProtoTagged "Element"

instance isEventTargetElement :: IsEventTarget Element where
    toEventTarget = unsafeCoerce
    fromEventTarget = readElement

instance isNodeElement :: IsNode Element where
    toNode = unsafeCoerce
    fromNode = readElement

-- | A class for subtypes of `Element`.
class IsNode element <= IsElement element where
    toElement :: element -> Element
    fromElement :: Element -> Maybe element

instance isElementElement :: IsElement Element where
    toElement = id
    fromElement = Just

instance isElementHtmlImageElement :: IsElement HtmlImageElement where
    toElement = unsafeCoerce
    fromElement = readHtmlImageElement

foreign import hasAttribute :: forall element effects. IsElement element =>
    String -> element -> Eff (dom :: DOM | effects) Boolean

foreign import setAttribute :: forall element effects. IsElement element =>
    String -> String -> element -> Eff (dom :: DOM | effects) Unit
