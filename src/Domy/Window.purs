module Domy.Window where

import Control.Monad.Eff (Eff, kind Effect)
import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Domy (DOM)
import Domy.HtmlDocument (HtmlDocument)
import Prelude (Unit, (<#>))

foreign import data Window :: Type

foreign import window :: forall effects. Eff (dom :: DOM | effects) Window

foreign import document :: forall effects.
    Window -> Eff (dom :: DOM | effects) HtmlDocument

foreign import data ALERT :: Effect

foreign import alert :: forall effects.
    String -> Window -> Eff (alert :: ALERT | effects) Unit

foreign import data WINDOW :: Effect

foreign import openImpl :: forall effects.
    String -> Window -> Eff (window :: WINDOW | effects) (Nullable Window)

open :: forall effects.
    String -> Window -> Eff (window :: WINDOW | effects) (Maybe Window)
open url window = openImpl url window <#> toMaybe
