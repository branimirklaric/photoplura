"use strict";

exports.textContent = function (isNode) {
    return function (node) {
        return function () {
            return node.textContent
        }
    }
}

exports.setTextContent = function (isNode) {
    return function (value) {
        return function (node) {
            return function () {
                node.textContent = value
                return {}
            }
        }
    }
}

exports.appendChild = function (isNode1) {
    return function (isNode2) {
        return function (parentNode) {
            return function (childNode) {
                return function () {
                    return parentNode.appendChild(childNode)
                }
            }
        }
    }
}

exports.parentNode = function (isNode1) {
    return function (isNode2) {
        return function (childNode) {
            return function () {
                return childNode.parentNode
            }
        }
    }
}
