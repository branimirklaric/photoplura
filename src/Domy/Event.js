"use strict";

exports.preventDefault = function (isEvent) {
    return function (event) {
        return function () {
            event.preventDefault()
        }
    }
}

exports.stopPropagation = function (isEvent) {
    return function (event) {
        return function () {
            event.stopPropagation()
        }
    }
}
