module Domy.Utils where

import Control.Monad.Except (runExcept)
import Data.Either (hush)
import Data.Foreign (Foreign, toForeign, unsafeReadTagged)
import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Prelude ((#))

unsafeReadTagged' :: forall anything object. String -> anything -> Maybe object
unsafeReadTagged' name object =
    object # toForeign # unsafeReadTagged name # runExcept # hush

foreign import unsafeReadProtoTaggedImpl :: forall node.
    String -> Foreign -> Nullable node

unsafeReadProtoTagged :: forall anything object. String -> anything -> Maybe object
unsafeReadProtoTagged name object =
    object # toForeign # unsafeReadProtoTaggedImpl name # toMaybe
