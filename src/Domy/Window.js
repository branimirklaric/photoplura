"use strict";

exports.window = function () {
    return window
}

exports.document = function (window) {
    return function () {
        return window.document
    }
}

exports.alert = function (text) {
    return function (window) {
        return function () {
            window.alert(text)
            return {}
        }
    }
}

exports.openImpl = function (url) {
    return function (window) {
        return function () {
            return window.open(url)
        }
    }
}
