module Domy.Document where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Domy (DOM)
import Domy.Element (class IsElement, Element, fromElement)
import Domy.HtmlCollection (HtmlCollection)
import Domy.HtmlDocument (HtmlDocument, readHtmlDocument)
import Domy.HtmlElements.HtmlBodyElement (HtmlBodyElement)
import Domy.Tag (Tag)
import Prelude (bind, pure, (#), (>>=))
import Unsafe.Coerce (unsafeCoerce)

foreign import data Document :: Type

class IsDocument document where
    toDocument :: document -> Document
    fromDocument :: Document -> Maybe document

instance isDocumentHtmlDocument :: IsDocument HtmlDocument where
    toDocument = unsafeCoerce
    fromDocument = readHtmlDocument

foreign import getElementByIdImpl :: forall document effects. IsDocument document =>
    String -> document -> Eff (dom :: DOM | effects) (Nullable Element)

getElementById :: forall element document effects.
    IsElement element => IsDocument document =>
    String -> document -> Eff (dom :: DOM | effects) (Maybe element)
getElementById id document = do
    element' <- getElementByIdImpl id document
    element' # toMaybe >>= fromElement # pure

foreign import getElementsByTagName :: forall element document effects.
    IsElement element => IsDocument document =>
    Tag element -> document -> Eff (dom :: DOM | effects) (HtmlCollection element)

foreign import createElement :: forall element document effects.
    IsElement element => IsDocument document =>
    Tag element -> document -> Eff (dom :: DOM | effects) element

foreign import body :: forall document effects. IsDocument document =>
    document -> Eff (dom :: DOM | effects) HtmlBodyElement
