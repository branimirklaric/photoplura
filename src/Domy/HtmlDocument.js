"use strict";

exports.location = function (document) {
    return function () {
        return document.location
    }
}

exports.activeElementImpl = function (document) {
    return function () {
        return document.activeElement
    }
}
