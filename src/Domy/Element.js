"use strict";

exports.hasAttribute = function (isElement) {
    return function (attribute) {
        return function (element) {
            return function () {
                return element.hasAttribute(attribute)
            }
        }
    }
}

exports.setAttribute = function (isElement) {
    return function (attribute) {
        return function (value) {
            return function (element) {
                return function () {
                    element.setAttribute(attribute, value)
                }
            }
        }
    }
}
