module Domy.MutationObserver where

import Control.Monad.Eff (Eff)
import Data.Foreign (Foreign)
import Data.Options (Option, Options, opt, options)
import Domy (DOM)
import Domy.Node (class IsNode)
import Prelude (Unit)

foreign import data MutationObserver :: Type

foreign import data MutationRecord :: Type

data MutationObserverInit

childList :: Option MutationObserverInit Boolean
childList = opt "childList"

subtree :: Option MutationObserverInit Boolean
subtree = opt "subtree"

foreign import mutationObserver :: forall effects.
    (Array MutationRecord -> MutationObserver -> Eff effects Unit) -> MutationObserver

foreign import observeImpl :: forall node effects. IsNode node =>
    node -> Foreign -> MutationObserver -> Eff (dom :: DOM | effects) Unit

observe :: forall node effects. IsNode node =>
    node -> Options MutationObserverInit -> MutationObserver -> Eff (dom :: DOM | effects) Unit
observe node init observer = observeImpl node (options init) observer
