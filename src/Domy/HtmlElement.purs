module Domy.HtmlElement where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(..))
import Data.Nullable (Nullable, toMaybe)
import Domy (DOM)
import Domy.CssStyleDeclaration (CssStyleDeclaration)
import Domy.Element (class IsElement)
import Domy.EventTarget (class IsEventTarget)
import Domy.HtmlElements.HtmlImageElement (HtmlImageElement, readHtmlImageElement)
import Domy.Node (class IsNode)
import Prelude (id, (#))
import Unsafe.Coerce (unsafeCoerce)

foreign import data HtmlElement :: Type

foreign import readHtmlElementImpl :: forall anything. anything -> Nullable HtmlElement

readHtmlElement :: forall anything. anything -> Maybe HtmlElement
readHtmlElement element = readHtmlElementImpl element # toMaybe

instance isEventTargetHtmlElement :: IsEventTarget HtmlElement where
    toEventTarget = unsafeCoerce
    fromEventTarget = readHtmlElement

instance isNodeHtmlElement :: IsNode HtmlElement where
    toNode = unsafeCoerce
    fromNode = readHtmlElement

instance isElementHtmlElement :: IsElement HtmlElement where
    toElement = unsafeCoerce
    fromElement = readHtmlElement

class IsElement element <= IsHtmlElement element where
    toHtmlElement :: element -> HtmlElement
    fromHtmlElement :: HtmlElement -> Maybe element

instance isHtmlElementHtmlElement :: IsHtmlElement HtmlElement where
    toHtmlElement = id
    fromHtmlElement = Just

instance isHtmlElementHtmlImageElement :: IsHtmlElement HtmlImageElement where
    toHtmlElement = unsafeCoerce
    fromHtmlElement = readHtmlImageElement

foreign import style :: forall element effects. IsHtmlElement element =>
    element -> Eff (dom :: DOM | effects) CssStyleDeclaration
