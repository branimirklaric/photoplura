"use strict";

exports.getElementByIdImpl = function (isDocument) {
    return function (id) {
        return function (document) {
            return function () {
                return document.getElementById(id)
            }
        }
    }
}

exports.getElementsByTagName = function (isElement) {
    return function (isDocument) {
        return function (tag) {
            return function (document) {
                return function () {
                    return document.getElementsByTagName(tag)
                }
            }
        }
    }
}

exports.createElement = function (isElement) {
    return function (isDocument) {
        return function (tagName) {
            return function (document) {
                return function () {
                    return document.createElement(tagName);
                }
            }
        }
    }
}

exports.body = function (isDocument) {
    return function (document) {
        return function () {
            return document.body
        }
    }
}
