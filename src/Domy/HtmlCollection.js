"use strict";

exports.length = function (collection) {
    return function () {
        return collection.length
    }
}

exports.toArray = function (collection) {
    return function () {
        return [].slice.call(collection)
    }
}

exports.itemImpl = function (index) {
    return function (collection) {
        return function () {
            return collection.item(index)
        }
    }
}

exports.namedItemImpl = function (name) {
    return function (collection) {
        return function () {
            return collection.namedItem(name)
        }
    }
}
