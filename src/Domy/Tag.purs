module Domy.Tag where

import Domy.HtmlElements.HtmlImageElement (HtmlImageElement)

newtype Tag element = Tag String

imgTag :: Tag HtmlImageElement
imgTag = Tag "img"
