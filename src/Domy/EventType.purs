module Domy.EventType where

import Domy.Event (Event)
import Domy.Events.KeyboardEvent (KeyboardEvent)
import Domy.Events.MouseEvent (MouseEvent)

newtype EventType event = EventType String

change :: EventType Event
change = EventType "change"

keydown :: EventType KeyboardEvent
keydown = EventType "keydown"
    
keypress :: EventType KeyboardEvent
keypress = EventType "keypress"
    
keyup :: EventType KeyboardEvent
keyup = EventType "keyup"

mouseover :: EventType MouseEvent
mouseover = EventType "mouseover"

mouseleave :: EventType MouseEvent
mouseleave = EventType "mouseleave"

mousemove :: EventType MouseEvent
mousemove = EventType "mousemove"

click :: EventType MouseEvent
click = EventType "click"
