module Domy.Event where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(..))
import Domy (DOM)
import Domy.Events.KeyboardEvent (KeyboardEvent)
import Domy.Events.MouseEvent (MouseEvent)
import Domy.Utils (unsafeReadTagged')
import Prelude (Unit)
import Unsafe.Coerce (unsafeCoerce)

foreign import data Event :: Type

class IsEvent event where
  toEvent :: event -> Event
  fromEvent :: Event -> Maybe event

instance isEventEvent :: IsEvent Event where
    toEvent = unsafeCoerce
    fromEvent = Just

instance isEventKeyboardEvent :: IsEvent KeyboardEvent where
    toEvent = unsafeCoerce
    fromEvent = unsafeReadTagged' "KeyboardEvent"

instance isEventMouseEvent :: IsEvent MouseEvent where
    toEvent = unsafeCoerce
    fromEvent = unsafeReadTagged' "MouseEvent"

foreign import preventDefault :: forall event effects. IsEvent event =>
    event -> Eff (dom :: DOM | effects) Unit

foreign import stopPropagation :: forall event effects. IsEvent event =>
    event -> Eff (dom :: DOM | effects) Unit
