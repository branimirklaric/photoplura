module Domy.HtmlCollection where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Domy (DOM)
import Prelude ((<#>))

-- | A generic collection (array-like object) of elements in document order.
-- | An HtmlCollection in the HTML DOM is live;
-- | it is automatically updated when the underlying document is changed.
foreign import data HtmlCollection :: Type -> Type

-- | The number of elements in a HtmlCollection.
foreign import length :: forall element effects.
    HtmlCollection element -> Eff (dom :: DOM | effects) Int

-- | The elements of an HtmlCollection represented in a static array.
foreign import toArray :: forall element effects.
    HtmlCollection element -> Eff (dom :: DOM | effects) (Array element)

foreign import itemImpl :: forall element effects.
    Int -> HtmlCollection element -> Eff (dom :: DOM | effects) (Nullable element)

-- | The element in a HtmlCollection at the specified index, or Nothing if no such
-- | element exists.
item :: forall element effects.
    Int -> HtmlCollection element -> Eff (dom :: DOM | effects) (Maybe element)
item index collection = itemImpl index collection <#> toMaybe

foreign import namedItemImpl :: forall element effects.
    String -> HtmlCollection element -> Eff (dom :: DOM | effects) (Nullable element)

-- | The first element with the specified name or ID in a HtmlCollection, or
-- | Nothing if no such element exists.
namedItem :: forall element effects.
    String -> HtmlCollection element -> Eff (dom :: DOM | effects) (Maybe element)
namedItem idOrName collection = namedItemImpl idOrName collection <#> toMaybe
