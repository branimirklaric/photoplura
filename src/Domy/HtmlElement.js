"use strict";

exports.readHtmlElementImpl = function (value) {
    var tag = Object.prototype.toString.call(value)
    if (tag.indexOf("[object HTML") === 0 && tag.indexOf("Element]") === tag.length - 8) {
        return value
    } else {
        return null
    }
}

exports.style = function (isHtmlElement) {
    return function (element) {
        return function () {
            return element.style
        }
    }
}
