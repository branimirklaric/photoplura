"use strict";

exports.toEventListener = function (isEvent) {
    return function (listener) {
        return function (event) {
            return listener(event)()
        }
    }
}

exports.addEventListener = function (isEvent) {
    return function (isEventTarget) {
        return function (eventType) {
            return function (listener) {
                return function (target) {
                    return function () {
                        target.addEventListener(eventType, listener)
                    }
                }
            }
        }
    }
}


exports.removeEventListener = function (isEvent) {
    return function (isEventTarget) {
        return function (eventType) {
            return function (listener) {
                return function (target) {
                    return function () {
                        target.removeEventListener(eventType, listener)
                    }
                }
            }
        }
    }
}

exports.dispatchEvent = function (isEvent) {
    return function (isEventTarget) {
        return function (event) {
            return function (target) {
                return function () {
                    return target.dispatchEvent(event)
                }
            }
        }
    }
}
