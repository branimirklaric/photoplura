"use strict";

exports.setEvercookie = function (key) {
    return function (value) {
        return function () {
            new Evercookie().set(key, value)
        }
    }
}

exports.getEvercookieForeign = function (key) {
    return function (callback) {
        return function () {
            new Evercookie().get(key, function (value) {
                callback(value)()
            })
        }
    }
}
