module Evercookie (setEvercookie, getEvercookie, EVERCOOKIE) where

import Control.Bind (join, (>>=))
import Control.Monad.Eff (Eff, kind Effect)
import Control.Monad.Except (runExcept)
import Data.Either (hush)
import Data.Foreign (Foreign, readString, readUndefined)
import Data.Function ((#))
import Data.Maybe (Maybe)
import Data.Traversable (traverse)
import Data.Unit (Unit)

foreign import data EVERCOOKIE :: Effect

foreign import setEvercookie :: forall effects.
    String -> String -> Eff (evercookie :: EVERCOOKIE | effects) Unit

foreign import getEvercookieForeign :: forall effects.
    String ->
    (Foreign -> Eff (effects) Unit) ->
    Eff (evercookie :: EVERCOOKIE | effects) Unit

getEvercookie :: forall effects.
    String ->
    (Maybe String -> Eff effects Unit) ->
    Eff (evercookie :: EVERCOOKIE | effects) Unit
getEvercookie key callback = getEvercookieForeign key \value ->
    value # readUndefined >>= traverse readString # runExcept # hush # join # callback
