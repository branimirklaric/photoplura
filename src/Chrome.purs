module Chrome where

import Control.Monad.Eff (kind Effect)

foreign import data CHROME :: Effect
