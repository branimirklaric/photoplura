module Options where

import Chrome (CHROME)
import Chrome.Storage (get, set, sync)
import Control.Monad.Eff (Eff)
import Domy (DOM)
import Domy.Event (preventDefault)
import Domy.EventTarget (addEventListener')
import Domy.EventType (change, keydown, keypress)
import Domy.Events.KeyboardEvent (code, key)
import Domy.Global (unsafeGetElementById)
import Domy.HtmlElements.HtmlInputElement (checked, setChecked, setValue)
import Network.HTTP.Affjax (AJAX)
import Options.Activation (prepareForActivation, showActivation)
import Options.Greeting (setDismissListener, showGreeting)
import Prelude (Unit, bind, discard, (#), (<>), (==), (>>=))

type Controls otherOptions = {
    showCursorImage :: Boolean,
    downloadCode :: String,
    downloadKey :: String,
    openCode :: String,
    openKey :: String,
    downloadAndCloseCode :: String,
    downloadAndCloseKey :: String,
    openAndCloseCode :: String,
    openAndCloseKey :: String
    | otherOptions
}

type Options = Controls (licenseKey :: String, greetingDismissed :: Boolean)

defaultOptions :: Options
defaultOptions = {
    licenseKey: "", greetingDismissed: false, showCursorImage: true,
    downloadCode: "KeyD", downloadKey: "D", openCode: "KeyF", openKey: "F",
    downloadAndCloseCode: "KeyE", downloadAndCloseKey: "E",
    openAndCloseCode: "KeyR", openAndCloseKey: "R" }

getOptions :: forall effects.
    (Options -> Eff (chrome :: CHROME | effects) Unit) ->
    Eff (chrome :: CHROME | effects) Unit
getOptions callback = sync >>= get defaultOptions callback

keyDisplay :: String -> String -> String
keyDisplay keyCode keyKey = keyCode <> " (actual: " <> keyKey <> ")"

setKeyOptionLister :: forall values effects.
    String ->
    (String -> String -> Record values) ->
    Eff (dom :: DOM, dom :: DOM, chrome :: CHROME | effects) Unit
setKeyOptionLister inputId optionsConstructor = do
    keyInput <- unsafeGetElementById inputId
    keyInput # addEventListener' keydown \keyboardEvent -> do
        preventDefault keyboardEvent
        setValue (keyDisplay (code keyboardEvent) (key keyboardEvent)) keyInput
        sync >>= set (optionsConstructor (code keyboardEvent) (key keyboardEvent))
    keyInput # addEventListener' keypress \keyboardEvent ->
        preventDefault keyboardEvent

setOptionsListeners :: forall effects. Eff (dom :: DOM, dom :: DOM, chrome :: CHROME | effects) Unit
setOptionsListeners = do
    cursorImageCheckbox <- unsafeGetElementById "cursorImage"
    cursorImageCheckbox # addEventListener' change \_ -> do
        showCursorImage <- checked cursorImageCheckbox
        sync >>= set { showCursorImage }

    setKeyOptionLister "downloadKey" { downloadCode: _, downloadKey: _ }
    setKeyOptionLister "openKey" { openCode: _, openKey: _ }
    setKeyOptionLister "downloadAndCloseKey" { downloadAndCloseCode: _, downloadAndCloseKey: _ }
    setKeyOptionLister "openAndCloseKey" { openAndCloseCode: _, openAndCloseKey: _ }

setControls :: forall otherOptions effects. Controls otherOptions -> Eff (dom :: DOM | effects) Unit
setControls controls = do
    unsafeGetElementById "cursorImage" >>= setChecked controls.showCursorImage
    unsafeGetElementById "downloadKey" >>= setValue (keyDisplay controls.downloadCode controls.downloadKey)
    unsafeGetElementById "openKey" >>= setValue (keyDisplay controls.openCode controls.openKey)
    unsafeGetElementById "downloadAndCloseKey" >>= setValue (keyDisplay controls.downloadAndCloseCode controls.downloadAndCloseKey)
    unsafeGetElementById "openAndCloseKey" >>= setValue (keyDisplay controls.openAndCloseCode controls.openAndCloseKey)

main :: forall effects. Eff (dom :: DOM, dom :: DOM, chrome :: CHROME, chrome :: CHROME, ajax :: AJAX | effects) Unit
main = do
    setDismissListener
    setOptionsListeners
    -- Check for license key in Chrome storage.
    getOptions \options -> do
        if options.licenseKey == ""
            -- If it's not there, Photoplura is not activated.
            then prepareForActivation
            -- If it's there, Photoplura is activated.
            else showActivation options.licenseKey
        showGreeting options.greetingDismissed
        setControls options
