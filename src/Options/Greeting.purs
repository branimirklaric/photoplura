module Options.Greeting where

import Chrome (CHROME)
import Chrome.Storage (set, sync)
import Control.Monad.Eff (Eff)
import Domy (DOM)
import Domy.Element (Element)
import Domy.EventTarget (addEventListener')
import Domy.EventType (click)
import Domy.Global (unsafeGetElementById)
import Prelude (Unit, discard, pure, unit, (>>=))
import Utils.CssHelpers (setDisplayBlockToId, setDisplayNoneToId)

setDismissListener :: forall effects. Eff (dom :: DOM, dom :: DOM, chrome :: CHROME | effects) Unit
setDismissListener =
    (unsafeGetElementById "dismiss" :: Eff (dom :: DOM, dom :: DOM, chrome :: CHROME | effects) Element)
    >>= addEventListener' click \_ -> do
        sync >>= set { greetingDismissed: true }
        setDisplayNoneToId "greeting"

showGreeting :: forall effects. Boolean -> Eff (dom :: DOM | effects) Unit
showGreeting greetingDismissed =
    if greetingDismissed then pure unit else setDisplayBlockToId "greeting"
