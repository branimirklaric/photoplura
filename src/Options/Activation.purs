module Options.Activation where

import Chrome (CHROME)
import Chrome.Storage (set, sync)
import Control.Monad.Aff (runAff_)
import Control.Monad.Eff (Eff)
import Data.Argonaut.Core (Json)
import Data.Either (Either(..))
import Data.FormURLEncoded (FormURLEncoded, fromArray)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Domy (DOM)
import Domy.Element (Element)
import Domy.EventTarget (addEventListener')
import Domy.EventType (click)
import Domy.Global (unsafeGetElementById)
import Domy.HtmlElements.HtmlInputElement (value)
import Domy.Node (setTextContent)
import Network.HTTP.Affjax (AJAX, AffjaxResponse, post)
import Network.HTTP.StatusCode (StatusCode(..))
import Prelude (Unit, const, discard, (#), ($), (==), (>>=))
import Utils.CssHelpers (setDisplayBlockToId, setDisplayNoneToId)

readLicenseKey :: forall effects. Eff (dom :: DOM | effects) String
readLicenseKey = unsafeGetElementById "licenseKeyInput" >>= value

gumroadRequestData :: String -> FormURLEncoded
gumroadRequestData licenseKey = fromArray $
    [ Tuple "product_permalink" (Just "oQznn"),
      Tuple "license_key" (Just licenseKey) ]

showActivation :: forall effects. String -> Eff (dom :: DOM | effects) Unit
showActivation licenseKey = do
    setDisplayNoneToId "notActivated"
    setDisplayNoneToId "error"
    setDisplayNoneToId "invalid"
    setDisplayBlockToId "activated"
    unsafeGetElementById "licenseKey" >>= \(p :: Element) -> setTextContent licenseKey p

checkLicenseKey :: forall effects.
    String -> Eff (dom :: DOM, chrome :: CHROME, chrome :: CHROME, ajax :: AJAX | effects) Unit
checkLicenseKey licenseKey =
    gumroadRequestData licenseKey
    # post "https://api.gumroad.com/v2/licenses/verify"
    # (runAff_ case _ of
        Left error -> setDisplayBlockToId "error"
        Right ({ status } :: AffjaxResponse Json) ->
            if status == StatusCode 200
                then do
                    sync >>= set { licenseKey }
                    showActivation licenseKey
                else
                    setDisplayBlockToId "invalid")

tryActivate :: forall effects.
    Eff (dom :: DOM, chrome :: CHROME, chrome :: CHROME, ajax :: AJAX | effects) Unit
tryActivate = do
    setDisplayNoneToId "error"
    setDisplayNoneToId "invalid"
    readLicenseKey >>= checkLicenseKey

prepareForActivation :: forall effects.
    Eff (dom :: DOM, dom :: DOM, chrome :: CHROME, chrome :: CHROME, ajax :: AJAX | effects) Unit
prepareForActivation = unsafeGetElementById "activateButton" >>= \(button :: Element) ->
    addEventListener' click (const tryActivate) button
