(IF EXIST extension (rmdir extension /Q /S & mkdir extension) ELSE (mkdir extension)) &^
pulp browserify --optimise --main Background | uglifyjs --output extension\Background.js &^
pulp browserify --optimise --main Content | uglifyjs --output extension\Content.js &^
pulp browserify --optimise --main Options | uglifyjs --output extension\Options.js &^
robocopy src extension manifest.json options.html Authentication.min.js &^
robocopy src\Icons extension\Icons
